﻿using ClothBazaar.Database;
using ClothBazaar.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ClothBazaar.Services
{
    public class ProductService
    {
        public void SaveProduct(Product product)
        {
            using(var context=new CBContext())
            {
                context.Products.Add(product);
                context.SaveChanges();
            }
        }
        public void UpdateProduct(Product product)
        {
            using(var context=new CBContext())
            {
                context.Entry(product).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }
        public void DeleteProduct(int ID)
        {
            using (var context = new CBContext())
            {
                var product=context.Products.Find(ID);
                context.Products.Remove(product);
                context.SaveChanges();
            }
        }
        public List<Product> GetProducts()
        {
            using(var context=new CBContext())
            {
                var product = context.Products.ToList();
                return context.Products.ToList();
                
            }
        }
        public Product GetProducts(int ID)
        {
            using(var context=new CBContext())
            {
                return context.Products.Find(ID);
            }
        }
    }
}